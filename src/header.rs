use yew::prelude::*;

#[function_component]
pub fn Header() -> Html {
		html! {
				<header>
				{ "Welcome to this Yew site." }
				</header>
		}
}
